#pragma once

#include "ofMain.h"
#include "ofxGui.h"

#define FFT_BANDS 1024
#define DECAY 0.9

class limit {
private:
	unsigned int maxVal;
	unsigned int minVal;
	unsigned int curVal;
public:
	limit(){}
	limit(
		unsigned int min,
		unsigned int max,
		unsigned int cur
	) : maxVal(max),
		minVal(min),
		curVal(cur) {}
	~limit(){}

	unsigned int getMaxVal() { return maxVal; }
	unsigned int getMinVal() { return minVal; }
	unsigned int getCurVal() { return curVal; }

	void setMaxVal(unsigned int f) { maxVal = f; }
	void setMinVal(unsigned int f) { minVal = f; }
	void setCurVal(unsigned int f) { curVal = f; }
};

class band {
private:
	limit lowLim;
	limit highLim;
	float avgVal;
public:
	limit getLowLim() { return lowLim; }
	limit getHighLim() { return highLim; }
	float getAvgVal() { return avgVal; }

	void setLowLim(limit l) { lowLim = l; }
	void setHighLim(limit l) { highLim = l; }
	void setAvgVal(float f) { avgVal = f; }

	void calcAvg(float* fptr) {
		float tmp = 0;
		for (int i = this->lowLim.getCurVal(); i < this->highLim.getCurVal(); i++)
			tmp += fptr[i];
		this->avgVal = tmp / (this->highLim.getCurVal() - this->lowLim.getCurVal());
	}
};

class ofApp : public ofBaseApp{

	public:
		void setup();
		void update();
		void draw();

		void keyPressed(int key);
		void keyReleased(int key);
		void mouseMoved(int x, int y );
		void mouseDragged(int x, int y, int button);
		void mousePressed(int x, int y, int button);
		void mouseReleased(int x, int y, int button);
		void mouseEntered(int x, int y);
		void mouseExited(int x, int y);
		void windowResized(int w, int h);
		void dragEvent(ofDragInfo dragInfo);
		void gotMessage(ofMessage msg);

		//input
		vector<ofSoundPlayer> vSound;
		ofSoundPlayer sound;
		float* fft;
		float* soundSpectrum;

		//spectrum view
		band lowBand;
		band midBand;
		band highBand;

		//Psychedelic Stuff
		float rotate;
		unsigned int ctr, r, g, b;
		bool rBool, gBool, bBool;
		void boxSeption(float, float);

		//GUI
		ofxPanel gui;
		ofxGuiGroup lowBandGroup;
		ofxGuiGroup midBandGroup;
		ofxGuiGroup highBandGroup;
		ofxGuiGroup selectBtns;
		ofxGuiGroup graphicsGroup;
		ofxGuiGroup audioGroup;

		ofxIntSlider lowBandSlider_low;
		ofxIntSlider lowBandSlider_high;
		ofxIntSlider midBandSlider_low;
		ofxIntSlider midBandSlider_high;
		ofxIntSlider highBandSlider_low;
		ofxIntSlider highBandSlider_high;

		//graphics
		ofxIntSlider scaleSlider;
		ofxIntSlider nBoxesSlider;
		ofxIntSlider sizeDeltaSlider;
		ofxFloatSlider rotDeltaSlider;
		ofxFloatSlider rotSpeedSlider;
		ofxButton chaosBtn;
		ofxButton colorPauseBtn;

		//audio control
		ofxFloatSlider volumeSlider;
		ofxButton playBtn;
		ofxButton pauseBtn;
		int audioAction;
		
		ofxButton btn1;
		ofxButton btn2;
		ofxButton btn3;
		ofxLabel lbl1;

		int state;  //state 1: subAmp,   state 2: midAmp,    state 3: highAmp
		int chaosVar;
		bool colorCycle;
		
		void btn1Pressed();
		void btn2Pressed();
		void btn3Pressed();
		void playPressed();
		void pausePressed();
		void chaosPressed();
		void colorPausePressed();
};



