#include "ofApp.h"

#define N_MODES 3
//--------------------------------------------------------------
void ofApp::setup() {
	sound.load("Avocuddle  Aint No Sunshine.mp3");
	//sound.load("Full_loop2.mp3");
	//sound.play();
	//sound.setLoop(true);

	//ofSetBackgroundColor(25, 25, 50);
	ofSetBackgroundColor(20);
	ofSetCircleResolution(64);

	lowBand.setLowLim(limit(0, 100, 0));
	lowBand.setHighLim(limit(100, 200, 200));

	midBand.setLowLim(limit(200, 300, 200));
	midBand.setHighLim(limit(300, 400, 400));

	highBand.setLowLim(limit(400, 500, 400));
	highBand.setHighLim(limit(500, 600, 600));

	// GUI erstmalig bauen (?)
	lowBandGroup.setup("lowBand");
	midBandGroup.setup("midBand");
	highBandGroup.setup("highBand");
	selectBtns.setup("bandSelect");
	graphicsGroup.setup("visuals");
	audioGroup.setup("audio");

	//lowBandGroup
	lowBandGroup.add(lowBandSlider_low.setup(
		"lower limit",
		lowBand.getLowLim().getCurVal(),
		lowBand.getLowLim().getMinVal(),
		lowBand.getLowLim().getMaxVal()						
	));
	lowBandGroup.add(lowBandSlider_high.setup(
		"upper limit",
		lowBand.getHighLim().getCurVal(),
		lowBand.getHighLim().getMinVal(),
		lowBand.getHighLim().getMaxVal()
	));
	
	//midBandGroup
	midBandGroup.add(midBandSlider_low.setup(
		"lower limit",
		midBand.getLowLim().getCurVal(),
		midBand.getLowLim().getMinVal(),
		midBand.getLowLim().getMaxVal()
	));
	midBandGroup.add(midBandSlider_high.setup(
		"upper limit",
		midBand.getHighLim().getCurVal(),
		midBand.getHighLim().getMinVal(),
		midBand.getHighLim().getMaxVal()
	));

	//highBandGroup
	highBandGroup.add(highBandSlider_low.setup(
		"lower limit",
		highBand.getLowLim().getCurVal(),
		highBand.getLowLim().getMinVal(),
		highBand.getLowLim().getMaxVal()
	));
	highBandGroup.add(highBandSlider_high.setup(
		"upper limit",
		highBand.getHighLim().getCurVal(),
		highBand.getHighLim().getMinVal(),
		highBand.getHighLim().getMaxVal()
	));

	// Buttons
	selectBtns.add(btn1.setup("lowBand"));
	selectBtns.add(btn2.setup("midBand"));
	selectBtns.add(btn3.setup("highBand"));
	selectBtns.add(lbl1.setup("curBand", "1"));

	//graphics
	graphicsGroup.add(scaleSlider.setup("scale", 1000, 100, 10000));
	graphicsGroup.add(nBoxesSlider.setup("boxCount", 5, 1, 150));
	graphicsGroup.add(sizeDeltaSlider.setup("sizeDelta", 10, 0 , 42));
	graphicsGroup.add(rotSpeedSlider.setup("rotationSpeed", 1, 0, 1));
	graphicsGroup.add(rotDeltaSlider.setup("rotationDelta", 1, 0, 1));
	graphicsGroup.add(colorPauseBtn.setup("colorCycle"));
	graphicsGroup.add(chaosBtn.setup("changeMode"));

	//audio
	audioGroup.add(volumeSlider.setup("volume", 0.4, 0, 1));
	audioGroup.add(playBtn.setup("play"));
	audioGroup.add(pauseBtn.setup("stop"));

	//add to panel
	gui.setup("Paul Haworth");
	gui.add(&lowBandGroup);
	gui.add(&midBandGroup);
	gui.add(&highBandGroup);
	gui.add(&selectBtns);
	gui.add(&graphicsGroup);
	gui.add(&audioGroup);

	//listeners
	btn1.addListener(this, &ofApp::btn1Pressed);
	btn2.addListener(this, &ofApp::btn2Pressed);
	btn3.addListener(this, &ofApp::btn3Pressed);
	playBtn.addListener(this, &ofApp::playPressed);
	pauseBtn.addListener(this, &ofApp::pausePressed);
	chaosBtn.addListener(this, &ofApp::chaosPressed);
	colorPauseBtn.addListener(this, &ofApp::colorPausePressed);

	//inits
	state = 1;
	rotate = 0;
	r = 1;
	g = (int)(256 / 3);
	b = (int)(256 * 2 / 3);
	rBool = 0;
	gBool = 0;
	bBool = 0;
	audioAction = 3;
	chaosVar = 0;
	colorCycle = 0;

	//setting fft to 0
	fft = new float[FFT_BANDS];
	for (int i = 0; i < FFT_BANDS; i++)
		fft[i] = 0;		
	cout << pow(2, (sizeof(int) - 1) * 8) << endl;
}

//--------------------------------------------------------------
void ofApp::update() {
	//sound stuff
	ofSoundUpdate();
	sound.setVolume(volumeSlider);
	switch (audioAction) {
	case 0:
		sound.play();
		audioAction = 2;
		break;
	case 1:
		sound.stop();
		audioAction = 2;
		for (int i = 0; i < FFT_BANDS; i++)
			fft[i] = 0;
		break;
	default:
		break;
	}

	//get whole spectrum
	soundSpectrum = ofSoundGetSpectrum(FFT_BANDS);
	for (int i = 0; i < FFT_BANDS; i++) {
		fft[i] *= DECAY;
		if (fft[i] < soundSpectrum[i])
			fft[i] = soundSpectrum[i];
	}
	
	//calculate Average
	lowBand.calcAvg(fft);
	midBand.calcAvg(fft);
	highBand.calcAvg(fft);


	//UPDATE VALUES IN CLASS
	//lowBand
	lowBand.setLowLim(limit(
		0,											//min
		lowBand.getHighLim().getCurVal(),			//max
		lowBandSlider_low							//cur
	));

	lowBand.setHighLim(limit(
		lowBand.getLowLim().getCurVal(),			//min
		midBand.getLowLim().getCurVal(),			//max
		lowBandSlider_high							//cur
	));

	//midBand
	midBand.setLowLim(limit(
		lowBand.getHighLim().getCurVal(),			//min
		midBand.getHighLim().getCurVal(),			//max
		midBandSlider_low							//cur
	));

	midBand.setHighLim(limit(
		midBand.getLowLim().getCurVal(),			//min
		highBand.getLowLim().getCurVal(),			//max
		midBandSlider_high							//cur
	));

	//highBand
	highBand.setLowLim(limit(
		midBand.getHighLim().getCurVal(),			//min
		highBand.getHighLim().getCurVal(),			//max
		highBandSlider_low							//cur
	));

	highBand.setHighLim(limit(
		highBand.getLowLim().getCurVal(),			//min
		FFT_BANDS,									//max
		highBandSlider_high							//cur
	));
	

	//UPDATE MIN/MAX ON SLIDERS
	//lowBand
	lowBandSlider_low.setMin(lowBand.getLowLim().getMinVal());
	lowBandSlider_low.setMax(lowBand.getLowLim().getMaxVal());

	lowBandSlider_high.setMin(lowBand.getHighLim().getMinVal());
	lowBandSlider_high.setMax(lowBand.getHighLim().getMaxVal());

	//midBand
	midBandSlider_low.setMin(midBand.getLowLim().getMinVal());
	midBandSlider_low.setMax(midBand.getLowLim().getMaxVal());

	midBandSlider_high.setMin(midBand.getHighLim().getMinVal());
	midBandSlider_high.setMax(midBand.getHighLim().getMaxVal());

	//highBand
	highBandSlider_low.setMin(highBand.getLowLim().getMinVal());
	highBandSlider_low.setMax(highBand.getLowLim().getMaxVal());

	highBandSlider_high.setMin(highBand.getHighLim().getMinVal());
	highBandSlider_high.setMax(highBand.getHighLim().getMaxVal());


	//OTHER STUFF
	

	//count up or down
	if (colorCycle)
	{
		if (rBool) r++;
		else r--;

		if (gBool) g++;
		else g--;

		if (bBool) b++;
		else b--;

		//switch counting direction
		if ((r == 255) || (r == 0))
			rBool = !rBool;

		if ((g == 255) || (g == 0))
			gBool = !gBool;

		if ((b == 255) || (b == 0))
			bBool = !bBool;
	}
}

//--------------------------------------------------------------
void ofApp::draw() {
	ofNoFill();

	//draw GUI
	gui.draw();

	//DRAW SPECTRUM
	float width = (float)(5 * 128) / FFT_BANDS;
	//All
	ofSetColor(100);
	for (int i = 0; i < FFT_BANDS; i++)
		ofDrawRectangle(100 + i * width, ofGetHeight() - 100, width, -(fft[i] * 200));

	//lowBand
	ofSetColor(255, 0, 0);
	for (int i = lowBand.getLowLim().getCurVal(); i < lowBand.getHighLim().getCurVal(); i++)
		ofDrawRectangle(100 + i * width, ofGetHeight() - 100, width, -(fft[i] * 200));

	//midBand
	ofSetColor(0, 255, 0);
	for (int i = midBand.getLowLim().getCurVal(); i < midBand.getHighLim().getCurVal(); i++)
		ofDrawRectangle(100 + i * width, ofGetHeight() - 100, width, -(fft[i] * 200));

	//highBand
	ofSetColor(0, 0, 255);
	for (int i = highBand.getLowLim().getCurVal(); i < highBand.getHighLim().getCurVal(); i++)
		ofDrawRectangle(100 + i * width, ofGetHeight() - 100, width, -(fft[i] * 200));

	//DRAW CUBE
	rotate += rotSpeedSlider;
	if (rotate > pow(2, (sizeof(int)-1)*8))
		rotate = 0;
	
	ofTranslate(ofGetWidth() / 2, ofGetHeight() / 2);
	ofRotateXDeg(rotate);
	ofRotateYDeg(rotate);
	ofRotateZDeg(rotate);
	if (chaosVar == 20) {
		for (int i = 0; i < nBoxesSlider+(int)(midBand.getAvgVal()*2000); i++) {
			switch (state) {
			case 1:
				boxSeption(-1 * rotDeltaSlider, lowBand.getAvgVal());
				break;
			case 2:
				boxSeption(-1 * rotDeltaSlider, midBand.getAvgVal());
				break;
			case 3:
				boxSeption(-1 * rotDeltaSlider, highBand.getAvgVal());
				break;
			default:
				break;
			}
		}
	} else {
		for (int i = 0; i < nBoxesSlider; i++) {
			switch (state + chaosVar) {
			case 1:
				boxSeption(-1 * rotDeltaSlider, lowBand.getAvgVal() - sizeDeltaSlider * (float)i / scaleSlider);
				break;
			case 2:
				boxSeption(-1 * rotDeltaSlider, midBand.getAvgVal() - sizeDeltaSlider * (float)i / scaleSlider);
				break;
			case 3:
				boxSeption(-1 * rotDeltaSlider, highBand.getAvgVal() - sizeDeltaSlider * (float)i / scaleSlider);
				break;
			case 11:
				boxSeption(rotate * rotDeltaSlider, lowBand.getAvgVal() - sizeDeltaSlider * (float)i / scaleSlider);
				break;
			case 12:
				boxSeption(rotate * rotDeltaSlider, midBand.getAvgVal() - sizeDeltaSlider * (float)i / scaleSlider);
				break;
			case 13:
				boxSeption(rotate * rotDeltaSlider, highBand.getAvgVal() - sizeDeltaSlider * (float)i / scaleSlider);
				break;
			default:
				break;
			}
		}
	}
}

void ofApp::boxSeption(float rot, float delta)
{
	ofRotateXDeg(rot);
	ofRotateYDeg(rot);
	ofRotateZDeg(rot);

	ofSetLineWidth(1);
	ofSetColor(r, g, b);

	ofDrawBox(0, 0, 0, 128 + delta * scaleSlider);
}


//--------------------------------------------------------------
void ofApp::btn1Pressed()
{
	state = 1;
	lbl1.setup("curBand", "1");
	lbl1.draw();
}
void ofApp::btn2Pressed()
{
	state = 2;
	lbl1.setup("curBand", "2");
	lbl1.draw();
}
void ofApp::btn3Pressed()
{
	state = 3;
	lbl1.setup("curBand", "3");
	lbl1.draw();
}
void ofApp::playPressed() {
	audioAction = 0;
}
void ofApp::pausePressed() {
	audioAction = 1;
}
void ofApp::chaosPressed() {
	chaosVar = (chaosVar + 10) % (10 * N_MODES);
}
void ofApp::colorPausePressed() {
	colorCycle = !colorCycle;
}









































// EVENTS
//--------------------------------------------------------------
void ofApp::keyPressed(int key) {

}

//--------------------------------------------------------------
void ofApp::keyReleased(int key) {

}

//--------------------------------------------------------------
void ofApp::mouseMoved(int x, int y) {

}

//--------------------------------------------------------------
void ofApp::mouseDragged(int x, int y, int button) {

}

//--------------------------------------------------------------
void ofApp::mousePressed(int x, int y, int button) {

}

//--------------------------------------------------------------
void ofApp::mouseReleased(int x, int y, int button) {

}

//--------------------------------------------------------------
void ofApp::mouseEntered(int x, int y) {

}

//--------------------------------------------------------------
void ofApp::mouseExited(int x, int y) {

}

//--------------------------------------------------------------
void ofApp::windowResized(int w, int h) {

}

//--------------------------------------------------------------
void ofApp::gotMessage(ofMessage msg) {

}

//--------------------------------------------------------------
void ofApp::dragEvent(ofDragInfo dragInfo) {
	if (dragInfo.files.size() > 0) {
		vSound.assign(dragInfo.files.size(), ofSoundPlayer());
		for (int i = 0; i < dragInfo.files.size(); i++) {
			vSound[i].load(dragInfo.files[i]);
			sound = vSound[i];
		}
	}
}
